import Link from 'next/link'
import React, { useState } from 'react'
import axios from 'axios'
import api from '../../api/api'

import {
  AccountCircle,
  Email,
  Lock,
  AssignmentInd,
  Phone
} from '@styled-icons/material-outlined'

import { FormWrapper, FormLink } from 'components/Form'
import Button from 'components/Button'
import TextField from 'components/TextField'

type dataUserCreate = {
  name: 'Luizera'
  email: 'Luizera'
  document: 'Luizera'
  password: 'Luizera'
  contact_phone: 'Luizera'
}

console.log(dataUserCreate.name)

function dataUserCreate(this: any) {
  this.state = dataUserCreate()
}

const FormSignUp = ({
  name,
  email,
  document,
  password,
  contact_phone
}: dataUserCreate) => {
  const [namee, setNamee] = useState('')
  console.log(name)
  return (
  <FormWrapper>
    {console.log(name)}
    <form onSubmit={dataUserCreate}>
      <TextField
        name="name"
        placeholder="Name"
        type="name"
        icon={<AccountCircle />}
        value={namee}
        onChange={e => setNamee(e.target.value)}
       // onCHange={e => dataUserCreate.name(e.target.value)}
      />
      <TextField
        name="email"
        placeholder="Email"
        type="email"
        icon={<Email />}
        value={email}
      />
      <TextField
        name="password"
        placeholder="Password"
        type="password"
        icon={<Lock />}
        value={password}
      />
      <TextField
        name="confirm-password"
        placeholder="Confirm password"
        type="password"
        icon={<Lock />}
        value={password}
      />
      <TextField
        name="Phone"
        placeholder="Phone"
        type="phone"
        icon={<Phone />}
        value={contact_phone}
      />
      <TextField
        name="Document"
        placeholder="Document"
        type="document"
        icon={<AssignmentInd />}
        value={document}
      />

      <Button size="large" fullWidth>
        Sign up nowwwww
      </Button>

      <FormLink>
        Already have an account?{' '}
        <Link href="/sign-in">
          <a>Sign in</a>
        </Link>
      </FormLink>
    </form>
  </FormWrapper>
)}

axios
  .post('http://localhost:3333/users', {
    name: 'name',
    email: 'Email32',
    document: 'document2',
    password: 'password2',
    contact_phone: 'Phone2'
  })
  .then(
    (response) => {
      console.log(response)
    },
    (error) => {
      console.log(error)
    }
  )

export default FormSignUp
