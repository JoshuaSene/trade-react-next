import { Story, Meta } from '@storybook/react/types-6-0'
import Ribborn from '.'

export default {
  title: 'Ribborn',
  component: Ribborn
} as Meta

export const Default: Story = () => <Ribborn />
