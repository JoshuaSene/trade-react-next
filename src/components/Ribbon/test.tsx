import { render, screen } from '@testing-library/react'

import Ribborn from '.'

describe('<Ribbon />', () => {
  it('should render the heading', () => {
    const { container } = render(<Ribborn />)

    expect(
      screen.getByRole('heading', { name: /Ribborn/i })
    ).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
