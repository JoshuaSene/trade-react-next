import Base from 'templates/Base'

import { BannerProps } from 'components/Banner'
import { HighlightProps } from 'components/Highlight'
import { Container } from 'components/Container'
import BannerSlider from 'components/BannerSlider'

import * as S from './styles'

export type HomeTemplateProps = {
  banners: BannerProps[]
  upcomingHighlight: HighlightProps
  freeHighlight: HighlightProps
}

const Home = ({
  banners,
  freeHighlight
}: HomeTemplateProps) => (
  <Base>
    <Container>
      <BannerSlider items={banners} />
    </Container>
  </Base>
)

export default Home
